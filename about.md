---
layout: page
permalink: /about/
title: About
tags: [about]
image:
  feature: abstract-7.jpg
  credit: dargadgetz
  creditlink:
share: true
---

##This blog is purely intended for the personal use of...myself.

######<center>Third Eye Code represents side projects meant for learning and my overall well-being.</center>

I have been in situations over the years where I wished I had documented something, but I didn't. and had to reinvent the wheel again.  So here I go...

<figure class="half">
	<img src="/images/cactus23.jpg" alt="">
</figure>