# Third Eye Code Blog

This is my first attempt at a blog.  I went with jekyll and forked the [HPSTER](http://github.com/mmistakes/hpstr-jekyll-theme) theme.  Thank you @mmistakes for the awesome work!

The theme is free and open source software, distributed under the [GNU General Public License](https://github.com/mmistakes/hpstr-jekyll-theme/blob/master/LICENSE) version 2 or later. So feel free to to modify this theme to suit your needs. 
